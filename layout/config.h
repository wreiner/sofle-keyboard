#pragma once

/* The way how "handedness" is decided (which half is which),
see https://docs.qmk.fm/#/feature_split_keyboard?id=setting-handedness
for more options.
*/


/* RGB Lighting */

// The pin connected to the data pin of the LEDs
#define RGB_DI_PIN D3

// The number of LEDs connected
#define RGBLED_NUM 16

// For split keyboards, the number of LEDs connected on each half directly wired to RGB_DI_PIN
#define RGBLED_SPLIT {8, 8}

// Lighting Layers
//#define RGBLIGHT_LAYERS

// 
//#define RGBLIGHT_EFFECT_BREATHING
//#define RGBLIGHT_EFFECT_RGB_TEST
//#define RGBLIGHT_EFFECT_TWINKLE

